//
// Created by evgenii on 15.05.2021.
//

#include "server.h"


int server_end_flag = 0;
int server_num_connections = 0;
atomic_int server_num_messages = 0;
atomic_long server_task_id = 0;
pthread_mutex_t server_socket_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t server_buffer_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t server_writer_cv = PTHREAD_COND_INITIALIZER;
pthread_cond_t server_reader_cv = PTHREAD_COND_INITIALIZER;
task_package_t server_buffer;
list_item_t server_list_root;

struct writer_thread_data {
    int fd;
    char user[16];
    int end_flag;
};

void *server_writer_thread(void *void_params) {
    pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
    struct writer_thread_data *thread_data = (struct writer_thread_data *) void_params;

    while (!server_end_flag) {
        pthread_cond_wait(&server_writer_cv, &lock);
        if (strcmp(thread_data->user, server_buffer.task.owner) == 0 && !thread_data->end_flag) {
            pthread_mutex_lock(&server_socket_lock);
            write(thread_data->fd, &server_buffer, sizeof(task_package_t));
            pthread_mutex_unlock(&server_socket_lock);
        }
        server_num_messages++;
        if (server_num_messages == server_num_connections) {
            pthread_cond_signal(&server_reader_cv);
        }
    }
    return NULL;
}

void *server_reader_thread(void *void_params) {
    int sock_fd = *(int *) void_params;
    char user[16];
    task_package_t task_package;
    pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;


    read(sock_fd, &task_package, sizeof(task_package_t));
    strcpy(user, task_package.task.owner);

    pthread_mutex_lock(&server_buffer_lock);

    //send tasks to user
    list_item_t *current_item = server_list_root.next;
    while (current_item != NULL) {
        if (strcmp(current_item->task.owner, user) == 0) {
            task_package.task = current_item->task;
            task_package.type = REQUEST_TYPE_ADD;
            pthread_mutex_lock(&server_socket_lock);
            write(sock_fd, &task_package, sizeof(task_package_t));
            pthread_mutex_unlock(&server_socket_lock);
        }
        current_item = current_item->next;
    }

    server_num_connections++;
    printf("Server accepted the client. Username is %s. Now %i connections\n", user, server_num_connections);
    pthread_t writer;
    struct writer_thread_data data;
    data.fd = sock_fd;
    data.end_flag = 0;
    strcpy(data.user, user);
    pthread_create(&writer, NULL, server_writer_thread, &data);
    pthread_mutex_unlock(&server_buffer_lock);

    while (recv(sock_fd, &task_package, sizeof(task_package_t), 0) > 0) {
        printf("From client:\n\t Title : %s", task_package.task.title);
        printf("\t Description: %s", task_package.task.description);
        struct tm tm = *localtime(&task_package.task.creation_time);
        printf("\t Created: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
               tm.tm_min, tm.tm_sec);
        strcpy(task_package.task.owner, user);
        if (task_package.type == REQUEST_TYPE_ADD) {
            task_package.task.creation_time = time(NULL);
            task_package.task.id = server_task_id++;
        }
        pthread_mutex_lock(&server_buffer_lock);
        process_request(task_package, &server_list_root);
        server_num_messages = 0;
        server_buffer = task_package;
        pthread_cond_broadcast(&server_writer_cv);
        pthread_cond_wait(&server_reader_cv, &lock);
        pthread_mutex_unlock(&server_buffer_lock);
    }
    data.end_flag = 1;
    pthread_mutex_lock(&server_buffer_lock);
    server_num_connections--;
    printf("User %s disconnected. Now %i connections\n", user, server_num_connections);
    pthread_cancel(writer);
    pthread_mutex_unlock(&server_buffer_lock);
    return NULL;
}

void *listener_thread(void *void_params) {
    int sock_fd, conn_fd, len;
    struct sockaddr_in serv_addr, cli;
    pthread_t client_reader;

    server_list_root.next = NULL;

    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd == -1) {
        printf("socket creation failed...\n");
        return NULL;
    } else
        printf("Socket successfully created..\n");
    memset(&serv_addr, 0, sizeof(serv_addr));


    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(PORT);

    if ((bind(sock_fd, (SA *) &serv_addr, sizeof(serv_addr))) != 0) {
        printf("socket bind failed...\n");
        return NULL;
    } else
        printf("Socket successfully bound..\n");

    if ((listen(sock_fd, 5)) != 0) {
        printf("Listen failed...\n");
        return NULL;
    } else
        printf("Server listening..\n");
    len = sizeof(cli);

    while (!server_end_flag) {
        conn_fd = accept(sock_fd, (SA *) &cli, &len);
        if (conn_fd < 0) {
            printf("server accept failed...\n");
            return NULL;
        } else {
            pthread_create(&client_reader, NULL, server_reader_thread, (void *) &conn_fd);
        }
    }

    close(sock_fd);
    return NULL;
}

int server_mode() {
    pthread_t listener;
    pthread_create(&listener, NULL, listener_thread, NULL);
    while (!server_end_flag) {
        char c = getchar();
        if (c == 'Q' || c == 'q') {
            server_end_flag = 1;
        }
    }
    return 0;
}