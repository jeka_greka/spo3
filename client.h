//
// Created by evgenii on 15.05.2021.
//

#ifndef SPO3_CLIENT_H
#define SPO3_CLIENT_H

#pragma once
#include "task_list.h"

int client_mode(char* username, char* host);

#endif //SPO3_CLIENT_H
