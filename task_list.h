//
// Created by evgenii on 11.05.2021.
//
#ifndef SPO3_TASK_LIST_H
#define SPO3_TASK_LIST_H

#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <time.h>
#include <stdatomic.h>

#define PORT 13000
#define SA struct sockaddr

typedef int request_type;
#define REQUEST_TYPE_ADD 0
#define REQUEST_TYPE_CHANGE 1
#define REQUEST_TYPE_DELETE 2
#define REQUEST_TYPE_CHANGE_LIST 3
#define REQUEST_TYPE_DELETE_LIST 4
#define REQUEST_TYPE_SERVICE 5

typedef struct {
    char owner[16];
    char list_name[32];
    char title[32];
    char description[256];
    long id;
    time_t creation_time;
    char deadline[64];
} task_t;

typedef struct {
    request_type type;
    task_t task;
} task_package_t;

typedef struct list_item {
    task_t task;
    struct list_item *next;
} list_item_t;

void process_request(task_package_t task_package, list_item_t *list_root);

void add(task_t task, list_item_t *list_root);

void change(task_t task, list_item_t *list_root);

void delete(task_t task, list_item_t *current_item);

void change_list(task_t task, list_item_t *list_root);

void delete_list(task_t task, list_item_t *current_item);

#endif //SPO3_TASK_LIST_H




