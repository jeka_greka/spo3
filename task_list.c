//
// Created by evgenii on 11.05.2021.
//

#include <stdlib.h>
#include <string.h>
#include "task_list.h"

void add(task_t task, list_item_t *list_root) {
    list_item_t *new_item = malloc(sizeof(list_item_t));
    new_item->task = task;
    if (list_root->next == NULL) {
        new_item->next = NULL;
        list_root->next = new_item;
    } else {
        new_item->next = list_root->next;
        list_root->next = new_item;
    }
}

void change(task_t task, list_item_t *list_root) {
    list_item_t *current_item = list_root->next;
    while (current_item != NULL) {
        if (current_item->task.id == task.id) {
            strcpy(current_item->task.list_name, task.list_name);
            strcpy(current_item->task.title, task.title);
            strcpy(current_item->task.description, task.description);
            strcpy(current_item->task.deadline, task.deadline);
            break;
        }
        current_item = current_item->next;
    }
}

void delete(task_t task, list_item_t *current_item) {
    while (current_item->next != NULL) {
        if (current_item->next->task.id == task.id) {
            list_item_t *item = current_item->next;
            current_item->next = current_item->next->next;
            free(item);
            break;
        }
        current_item = current_item->next;
    }
}

void change_list(task_t task, list_item_t *list_root) {
    list_item_t *current_item = list_root->next;
    while (current_item != NULL) {
        if (strcmp(current_item->task.list_name, task.list_name) == 0 &&
            strcmp(current_item->task.owner, task.owner) == 0) {
            strcpy(current_item->task.list_name, task.title);
        }
        current_item = current_item->next;
    }
}

void delete_list(task_t task, list_item_t *current_item) {
    while (current_item->next != NULL) {
        if (strcmp(current_item->next->task.list_name, task.list_name) == 0 &&
            strcmp(current_item->next->task.owner, task.owner) == 0) {
            list_item_t *item = current_item->next;
            current_item->next = current_item->next->next;
            free(item);
        } else {
            current_item = current_item->next;
        }
    }
}

void process_request(task_package_t task_package, list_item_t *list_root) {
    switch (task_package.type) {
        case REQUEST_TYPE_ADD: {
            add(task_package.task, list_root);
            return;
        }
        case REQUEST_TYPE_CHANGE: {
            change(task_package.task, list_root);
            return;
        }
        case REQUEST_TYPE_DELETE: {
            delete(task_package.task, list_root);
            return;
        }
        case REQUEST_TYPE_CHANGE_LIST: {
            change_list(task_package.task, list_root);
            return;
        }
        case REQUEST_TYPE_DELETE_LIST: {
            delete_list(task_package.task, list_root);
            return;
        }
        default:
            return;
    }
}
