//
// Created by evgenii on 15.05.2021.
//

#include <ncurses.h>
#include "client.h"

int end_flag_client = 0;
int client_socket_fd;

WINDOW *windows[4];
char *titles[4] = {"Lists", "Tasks", "Details", "Edit"};
int current_window = 0;

int current_list_number = 0;
int total_lists = 0;
task_t current_list;

int current_task_number = 0;
int total_tasks = 0;
task_t current_task;

int current_detail_number = 0;

char edit_buffer[256];


list_item_t task_list_root;
list_item_t list_list_root;

void refresh_edit_window();

void refresh_list_window();

void refresh_task_window();

void refresh_details_window();

void refresh_list_list() {
    list_item_t *current_item = &list_list_root;
    list_item_t *current_task_item = &task_list_root;
    while (current_item->next != NULL) {
        list_item_t *item = current_item->next;
        current_item->next = current_item->next->next;
        free(item);
    }
    while (current_task_item != NULL) {
        current_item = list_list_root.next;
        int flag = 0;
        while (current_item != NULL) {
            if (strcmp(current_item->task.list_name, current_task_item->task.list_name) == 0) {
                flag = 1;
                break;
            }
            current_item = current_item->next;
        }
        if (!flag) {
            add(current_task_item->task, &list_list_root);
        }
        current_task_item = current_task_item->next;
    }
}

void *client_reader_thread() {
    task_package_t task_package;
    while (!end_flag_client) {
        read(client_socket_fd, &task_package, sizeof(task_package_t));
        process_request(task_package, &task_list_root);
        if (task_package.type == REQUEST_TYPE_DELETE_LIST || task_package.type == REQUEST_TYPE_DELETE) {
            current_task.id = -1;
            current_task_number = 0;
            current_list_number = 0;
        }
        refresh_list_list();
        refresh_list_window();
    }
    return NULL;
}

void set_up_socket(char *username, char *host) {
    struct sockaddr_in serv_addr;

    client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket_fd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    memset(&serv_addr, 0, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(host);
    serv_addr.sin_port = htons(PORT);

    if (connect(client_socket_fd, (SA *) &serv_addr, sizeof(serv_addr)) != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    }

    pthread_t receiver;
    pthread_create(&receiver, NULL, client_reader_thread, NULL);
    task_package_t task_package;

    strcpy(task_package.task.owner, username);
    task_package.type = REQUEST_TYPE_SERVICE;
    write(client_socket_fd, &task_package, sizeof(task_package_t));
}

void send_request(int type, task_t task) {
    task_package_t task_package;
    task_package.type = type;
    task_package.task = task;
    write(client_socket_fd, &task_package, sizeof(task_package_t));
}

void refresh_list_window() {
    wclear(windows[0]);
    current_window == 0 ? box(windows[0], '|', '-') : box(windows[0], 0, 0);
    wprintw(windows[0], titles[0]);
    int counter = 0;
    list_item_t *item = list_list_root.next;
    while (item != NULL) {
        if (counter == current_list_number) {
            mvwprintw(windows[0], counter + 2, 1, ">");
            current_list = item->task;
        }
        mvwprintw(windows[0], counter + 2, 2, item->task.list_name);
        item = item->next;
        counter++;
    }
    total_lists = counter - 1;
    wrefresh(windows[0]);
    refresh_task_window();
}

void refresh_task_window() {
    wclear(windows[1]);
    current_window == 1 ? box(windows[1], '|', '-') : box(windows[1], 0, 0);
    wprintw(windows[1], titles[1]);
    int counter = 0;
    current_task.id = -1;
    list_item_t *item = task_list_root.next;
    while (item != NULL) {
        if (strcmp(item->task.list_name, current_list.list_name) == 0) {
            if (counter == current_task_number) {
                mvwprintw(windows[1], counter + 2, 1, ">");
                current_task = item->task;
            }
            mvwprintw(windows[1], counter + 2, 2, item->task.title);
            counter++;
        }
        item = item->next;
    }
    total_tasks = counter;
    wrefresh(windows[1]);
    refresh_details_window();
}

void refresh_details_window() {
    wclear(windows[2]);
    current_window == 2 ? box(windows[2], '|', '-') : box(windows[2], 0, 0);
    wprintw(windows[2], titles[2]);
    struct tm tm;
    char buff[32];
    if (current_task.id != -1) {
        mvwprintw(windows[2], 2, 2, "Title:");
        mvwprintw(windows[2], 2, 15, current_task.title);

        mvwprintw(windows[2], 3, 2, "List:");
        mvwprintw(windows[2], 3, 15, current_task.list_name);

        mvwprintw(windows[2], 4, 2, "Description:");
        mvwprintw(windows[2], 4, 15, current_task.description);

        tm = *localtime(&current_task.creation_time);
        sprintf(buff, "Created:     %d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
                tm.tm_hour, tm.tm_min, tm.tm_sec);
        mvwprintw(windows[2], 5, 2, buff);


        mvwprintw(windows[2], 6, 2, "Deadline:");
        mvwprintw(windows[2], 6, 15, current_task.deadline);


        mvwprintw(windows[2], current_detail_number + 2, 1, ">");
        if (current_detail_number == 0) {
            strcpy(edit_buffer, current_task.title);
        }
        if (current_detail_number == 1) {
            strcpy(edit_buffer, current_task.list_name);
        }
        if (current_detail_number == 2) {
            strcpy(edit_buffer, current_task.description);
        }

        if (current_detail_number == 4){
            strcpy(edit_buffer, current_task.deadline);
        }
    }
    wrefresh(windows[2]);
    refresh_edit_window();
}

void refresh_edit_window() {
    wclear(windows[3]);
    current_window == 3 ? box(windows[3], '|', '-') : box(windows[3], 0, 0);
    wprintw(windows[3], titles[3]);
    mvwprintw(windows[3], 2, 2, edit_buffer);
    wrefresh(windows[3]);
}


int client_mode(char *username, char *host) {

    task_list_root.next = NULL;
    list_list_root.next = NULL;

    set_up_socket(username, host);

    const int listWW = 20;
    const int editWH = 7;

    initscr();
    noecho();
    raw();
    curs_set(0);
    refresh();

    windows[0] = newwin(LINES - editWH, listWW, 0, 0);
    windows[1] = newwin(LINES - editWH, listWW, 0, listWW);
    windows[2] = newwin(LINES - editWH, COLS - listWW * 2, 0, listWW * 2);
    windows[3] = newwin(editWH, COLS, LINES - editWH, 0);
    refresh_list_window();

    while (!end_flag_client) {
        char c = getch();
        if (c == 27) {
            end_flag_client = 1;
        } else if (c == 127 && current_window == 3) {
            edit_buffer[strlen(edit_buffer) - 1] = '\0';
            refresh_edit_window();
        } else if (c == '\n') {
            if (current_window == 2) {
                if (current_task.id >= 0) {
                    send_request(REQUEST_TYPE_CHANGE, current_task);
                } else {
                    send_request(REQUEST_TYPE_ADD, current_task);
                }
            }
            if (current_window == 3) {
                if (current_detail_number == 0) {
                    strcpy(current_task.title, edit_buffer);
                }
                if (current_detail_number == 1) {
                    strcpy(current_task.list_name, edit_buffer);
                }
                if (current_detail_number == 2) {
                    strcpy(current_task.description, edit_buffer);
                }
                if (current_detail_number == 4) {
                    strcpy(current_task.deadline, edit_buffer);
                }
                current_window = 2;
                refresh_details_window();
            }
        } else if (c == '\t') {
            current_window = (current_window + 1) % (current_task.id == -1 ? 2 : 4);
            if (current_window == 3) {
                refresh_details_window();
            } else {
                refresh_list_window();
            }
        } else if (current_window == 3) {
            strncat(edit_buffer, &c, 1);
            refresh_edit_window();
        } else if (c == 'w' || c == 'W') {
            if (current_window == 0) {
                current_list_number = current_list_number == 0 ? total_lists - 1 : current_list_number - 1;
                refresh_list_window();
            }
            if (current_window == 1) {
                current_task_number = current_task_number == 0 ? total_tasks - 1 : current_task_number - 1;
                refresh_task_window();
            }
            if (current_window == 2) {
                current_detail_number = current_detail_number == 0 ? 4 : current_detail_number - 1;
                refresh_details_window();
            }
        } else if (c == 's' || c == 'S') {
            if (current_window == 0) {
                current_list_number = (current_list_number + 1) % total_lists;
                refresh_list_window();
            }
            if (current_window == 1) {
                current_task_number = (current_task_number + 1) % total_tasks;
                refresh_task_window();
            }
            if (current_window == 2) {
                current_detail_number = (current_detail_number + 1) % 5;
                refresh_details_window();
            }
        } else if (c == 'd' || c == 'D') {
            if (current_window == 0) {
                send_request(REQUEST_TYPE_DELETE_LIST, current_list);
            }
            if (current_window == 1) {
                send_request(REQUEST_TYPE_DELETE, current_task);
            }
        } else if ((c == 'i' || c == 'I') && (current_window == 1 || current_window == 0)) {
            current_task.id = -2;
            strcpy(current_task.title, "");
            strcpy(current_task.description, "");
            strcpy(current_task.list_name, "");
            strcpy(current_task.deadline, "");
            current_task.creation_time = time(NULL);
            current_window = 2;
            refresh_details_window();
        }
    }
    endwin();
    return 0;
}