cmake_minimum_required(VERSION 3.19)
project(spo3 C)

set(CMAKE_C_STANDARD 99)

add_executable(spo3 main.c server.c client.c task_list.c)

target_link_libraries(spo3 pthread ncurses)